﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpRatCon
{
    public class SqlManager
    {
        // sql connection info
        private static string sqlEmpRatesUser = "EmpRates";
        private static string sqlEmpRatesPass = "fK&74&*hhO9^";
        private string _sqlServerName = @"INV-SQLCLUST03\INVINST03";
        private string _sqlDatabaseName = "RelReporter";

        private string _tableName;
        SqlConnection _sqlconn;
        SqlDataAdapter _adapter;

        public SqlManager(string tableName, ref DataTable dt)
        {
            _tableName = tableName;
            InitSqlConnection();
            _sqlconn.Open();
            _adapter = new SqlDataAdapter("SELECT * FROM " + _tableName + " ORDER BY EmployeeID, ProjectReference, WorkLogType", _sqlconn);
            _adapter.Fill(dt);
            InitAdapter();
            _sqlconn.Close();
        }

        private void InitSqlConnection()
        {
            _sqlconn = new SqlConnection(@"Server=" + _sqlServerName + ";Database=" + _sqlDatabaseName + ";User Id=" + sqlEmpRatesUser + ";Password=" + sqlEmpRatesPass);
        }

        private void InitAdapter()
        {
            // build the commands
            SqlCommand command = new SqlCommand("UPDATE " + _tableName + " SET Rate = @Rate, " +
                "EmployeeID = @EmployeeID, " +
                "ProjectReference = @ProjectReference, " +
                "WorkLogType = @WorkLogType " +
                "WHERE ID = @ID", _sqlconn
                );
            command.Parameters.Add("@ProjectReference", SqlDbType.NVarChar, 7, "ProjectReference");
            command.Parameters.Add("@WorkLogType", SqlDbType.NVarChar, 40, "WorkLogType");
            command.Parameters.Add("@EmployeeID", SqlDbType.Int, 9999999, "EmployeeID");
            command.Parameters.Add("@Rate", SqlDbType.Decimal, 9999999, "Rate");
            command.Parameters.Add("@ID", SqlDbType.Int, 9999999, "Id");
            _adapter.UpdateCommand = command;

            command = new SqlCommand("DELETE FROM " + _tableName + " WHERE ID = @id", _sqlconn);
            command.Parameters.Add("@ID", SqlDbType.Int, 9999999, "Id");
            _adapter.DeleteCommand = command;

            command = new SqlCommand("INSERT INTO " + _tableName + " (EmployeeID, ProjectReference, WorkLogType, Rate) " +
                "VALUES (@EmployeeID, @ProjectReference, @WorkLogType, @Rate)", _sqlconn);
            _adapter.InsertCommand = command;
        }

        public int GetNextId()
        {
            _sqlconn.Open();
            SqlCommand cmdGetNextId = new SqlCommand("SELECT IDENT_CURRENT('" + _tableName + "')", _sqlconn);
            int newid = Convert.ToInt32(cmdGetNextId.ExecuteScalar());
            return newid + 1;
        }

        public void NewRow(string projRef, string workType, string empID, string rate)
        {
            _sqlconn.Open();
            _adapter.InsertCommand.Parameters.Clear();
            _adapter.InsertCommand.Parameters.AddWithValue("@ProjectReference", projRef);
            _adapter.InsertCommand.Parameters.AddWithValue("@WorkLogType", workType);
            _adapter.InsertCommand.Parameters.AddWithValue("@EmployeeID", empID);
            _adapter.InsertCommand.Parameters.AddWithValue("@Rate", rate);
            _adapter.InsertCommand.ExecuteNonQuery();
        }

        public void Update(DataTable dt)
        {
            _adapter.Update(dt);
        }

        public void CloseConn()
        {
            _sqlconn.Close();
        }
    }
}
