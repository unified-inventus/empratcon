﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using TerisUtilities.RecordFiles;

namespace EmpRatCon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SqlManager _sql;
        private DataTable _dt;

        public string SqlServerName;
        public string SqlDatabaseName;

        // debug or no
        private string tableName;

        public MainWindow(string newTableName, string serverName, string databaseName)
        {
            tableName = newTableName;
            InitializeComponent();
            //dgMain.CanUserDeleteRows = false; // prevent any bad mistakes this way
            FillData();
        }

        #region DataTable

        private static string dtID = "ID";
        private static string dtWorkType = "WorkLogType";
        private static string dtProjectRef = "ProjectReference";
        private static string dtEmployeeID = "EmployeeID";
        private static string dtRate = "Rate";

        private void FillData()
        {
            try
            {
                _dt = new DataTable();
                _sql = new SqlManager(tableName, ref _dt);
                this.DataContext = _dt.DefaultView;

                _dt.RowChanged += new DataRowChangeEventHandler(DataGridRowModified);
                _dt.RowDeleted += new DataRowChangeEventHandler(DataGridRowModified);
                _dt.TableNewRow += new DataTableNewRowEventHandler(DataGridRowAdded);
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not initialize: " + e.Message);
            }
        }

        private void AddRow(string projRef, string workLogType, string employeeID, string rate)
        {
            DataRow row = _dt.NewRow();
            row[dtProjectRef] = projRef;
            row[dtWorkType] = workLogType;
            row[dtEmployeeID] = employeeID;
            row[dtRate] = rate;
            _dt.Rows.Add(row);
            this.DataContext = _dt.DefaultView;
        }

        private void DataGridRowAdded(object sender, DataTableNewRowEventArgs e)
        {
            try
            {
                e.Row[dtID] = _sql.GetNextId();
            }
            catch (Exception exc)
            {
                MessageBox.Show("Update Failed: " + exc.Message);
                e.Row.RejectChanges();
            }
            finally
            {
                _sql.CloseConn();
            }
        }

        void DataGridRowModified(object sender, DataRowChangeEventArgs e)
        {
            try
            {
                // Begin by validating the output
                if (e.Action == DataRowAction.Add)
                {
                    _sql.NewRow(e.Row[dtProjectRef].ToString(), e.Row[dtWorkType].ToString(), e.Row[dtEmployeeID].ToString(), e.Row[dtRate].ToString());
                    _dt.AcceptChanges();
                }
                else if ((e.Action == DataRowAction.Change) && (e.Row[dtProjectRef].ToString().Length != 7))
                {
                    MessageBox.Show("ProjectReference should be 7 characters exactly.");
                    e.Row.RejectChanges();
                }
                else if (e.Action == DataRowAction.Delete)
                {
                    MessageBoxResult mbr = MessageBox.Show("Do you want to delete this record?", "Warning: Deletion", MessageBoxButton.YesNoCancel);
                    if (mbr == MessageBoxResult.Yes)
                        _sql.Update(_dt);
                    else
                        e.Row.RejectChanges();
                }
                else
                {
                    if (e.Action == DataRowAction.Change)
                    {
                        _sql.Update(_dt);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Update Failed: " + exc.Message);
                e.Row.RejectChanges();
            }
            finally
            {
                _sql.CloseConn();
            }
        }

        #endregion DataTable

        #region ProcessCSV

        // fields expected in the CSV
        private static string pCsvWorkType = "Work Type";
        private static string pCsvEmployeeID = "Employee ID";
        private static string pCsvProjectRef = "Project Reference";
        private static string pCsvProjectName = "Project Name";
        private static string pCsvDuration = "Duration";
        private static string pCsvStartTime = "Start Time";
        private static string pCsvWorkLogQc = "Work Log QC";

        private void miProcessCsv_Click(object sender, RoutedEventArgs e)
        {
            // user selects source file
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Title = "Open AMP Rate CSV file";
            ofd.Filter = "CSV file (*.csv)|*.csv";
            bool? result = false;
            try
            {
                result = ofd.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error encountered: " + exc.Message);
            }
            if (result != true)
                return;

            // user selects destination file
            Microsoft.Win32.SaveFileDialog sfd = new Microsoft.Win32.SaveFileDialog();
            sfd.Title = "Save New CSV file";
            sfd.Filter = "CSV file (*.csv)|*.csv";
            result = false;
            try
            {
                result = sfd.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error encountered: " + exc.Message);
            }
            if (result != true)
                return;

            // do the work
            if (ProcessCsvWork(ofd.FileName, sfd.FileName) == true)
            {
                MessageBox.Show("All Done!");
            }
        }

        private bool ProcessCsvWork(string inputFilename, string outputFilename)
        {
            // first confirm the correct fields are present
            List<string> fieldHeaders = RecordFile.GetFileHeadersList(inputFilename);
            List<string> missingFields = new List<string>();
            foreach (string field in processCsvRequiredFieldList())
            {
                if (fieldHeaders.Contains(field) == false)
                    missingFields.Add(field);
            }
            if (missingFields.Count > 0)
            {
                MessageBox.Show("Missing the following fields in the source CSV: " + String.Join(", ", missingFields));
                return false;
            }

            // intialize input and output files
            RecordFile inputFile = RecordFileFactory.OpenFile(inputFilename, true);
            RecordWriter outputFile = RecordWriterFactory.GetRecordWriter(outputFilename, Encoding.ASCII);
            outputFile.WriteLine(new string[] { "EmployeeID", "DurationTotal", "Rate" });
            RecordWriter outputDetailFile = RecordWriterFactory.GetRecordWriter(outputFilename + ".details.csv", Encoding.ASCII);
            outputDetailFile.WriteLine(new string[] { "EmployeeID", "ProjectReference", "ProjectName", "WorkLogType", "StartTime", "Duration", "DurationDec", "Rate", "Total", "WorkLogQC", "Notes" });
            RecordWriter outputErrorFile = RecordWriterFactory.GetRecordWriter(outputFilename + ".errors.csv", Encoding.ASCII);
            outputErrorFile.WriteLine(new string[] { "EmployeeID", "ProjectReference", "WorkLogType", "Duration", "Notes" });
            Dictionary<string, Dictionary<string, double>> empDict = new Dictionary<string, Dictionary<string, double>>();

            // match up
            foreach (RecordEntry inputRecord in inputFile)
            {
                // Request from Stephen 7/25, convert WorkLogQC = "QC'ed but Not Submitted" into "Approved to Pay/Bill"
                string workLogQcVal = inputRecord[pCsvWorkLogQc];
                if (workLogQcVal == "QC'ed but Not Submitted")
                    workLogQcVal = "Approved to Pay/Bill";

                // first try parsing duration - if we can't convert, then mark as error
                double duration;
                try
                {
                    DateTime dt = DateTime.Parse(inputRecord[pCsvDuration]);
                    DateTime dtZero = DateTime.Parse("0:00:00");
                    duration = (dt - dtZero).TotalHours;
                }
                catch (Exception exc)
                {
                    outputErrorFile.WriteLine(new string[] { inputRecord[pCsvEmployeeID], inputRecord[pCsvProjectRef], inputRecord[pCsvWorkType], inputRecord[pCsvDuration], "Error: could not convert duration." });
                    outputDetailFile.WriteLine(new string[] { inputRecord[pCsvEmployeeID], inputRecord[pCsvProjectRef], inputRecord[pCsvProjectName], inputRecord[pCsvWorkType], inputRecord[pCsvStartTime], inputRecord[pCsvDuration], "", "", "", workLogQcVal, "Error: could not convert duration." });
                    continue;
                }

                // now try determining the rate
                decimal rate = 0;
                string rateString = "";
                string selectString;
                if (inputRecord[pCsvEmployeeID] == "" || inputRecord[pCsvProjectRef] == "" || inputRecord[pCsvWorkType] == "")
                    selectString = "1 = 0"; // select nothing
                else
                {
                    selectString = "EmployeeID = '" + inputRecord[pCsvEmployeeID] + "' AND " +
                    "ProjectReference = '" + inputRecord[pCsvProjectRef] + "' AND " +
                    "WorkLogType = '" + inputRecord[pCsvWorkType] + "'";
                }
                DataRow[] rows = _dt.Select(selectString);
                if (rows.Count() == 0) // no matches
                {
                    outputErrorFile.WriteLine(new string[] { inputRecord[pCsvEmployeeID], inputRecord[pCsvProjectRef], inputRecord[pCsvWorkType], inputRecord[pCsvDuration], "Error: could not locate corresponding rate." });
                }
                else // we have a match
                {
                    rate = Convert.ToDecimal(rows[0]["Rate"]);
                    rateString = rate.ToString("0.00");
                }
                string total = "";
                if (rateString != "")
                {
                    total = ((decimal)duration * rate).ToString("0.00");
                }

                // Request from Melanie 7/24 for QC to WorkLogQC field
                string workLogQcWarning = "";
                if (workLogQcVal != "Approved to Pay/Bill")
                {
                    outputErrorFile.WriteLine(new string[] { inputRecord[pCsvEmployeeID], inputRecord[pCsvProjectRef], inputRecord[pCsvWorkType], inputRecord[pCsvDuration], "Warning: Work Log QC field not set to Approved" });
                    workLogQcWarning = "Warning: Work Log QC field not set to Approved";
                }
                outputDetailFile.WriteLine(new string[] { inputRecord[pCsvEmployeeID], inputRecord[pCsvProjectRef], inputRecord[pCsvProjectName], inputRecord[pCsvWorkType], inputRecord[pCsvStartTime], inputRecord[pCsvDuration], duration.ToString("0.00"), rateString, total, workLogQcVal, rateString == "" ? "Error: could not locate corresponding rate." : workLogQcWarning });

                // update the employee list
                if (rateString == "") // skip if no rate
                    continue;
                if (!empDict.ContainsKey(inputRecord[pCsvEmployeeID]))
                    empDict.Add(inputRecord[pCsvEmployeeID], new Dictionary<string, double>());
                if (empDict[inputRecord[pCsvEmployeeID]].ContainsKey(rateString))
                    empDict[inputRecord[pCsvEmployeeID]][rateString] += duration;
                else
                    empDict[inputRecord[pCsvEmployeeID]].Add(rateString, duration);
            }

            // Write the final sum file
            foreach (KeyValuePair<string, Dictionary<string, double>> employee in empDict)
            {
                foreach (KeyValuePair<string, double> rate in employee.Value)
                {
                    // have to convert the duration back to timespan again
                    TimeSpan dts = TimeSpan.FromHours(rate.Value);
                    string durationTotalString = (dts.Hours + (dts.Days * 24)).ToString("0") + ":" + dts.Minutes.ToString("00") + ":" + dts.Seconds.ToString("00");
                    outputFile.WriteLine(new string[] { employee.Key, durationTotalString, rate.Key });
                }
            }

            outputDetailFile.Dispose();
            outputErrorFile.Dispose();
            outputFile.Dispose();

            return true;
        }

        private List<string> processCsvRequiredFieldList()
        {
            List<string> fields = new List<string>
            {
                pCsvDuration,
                pCsvEmployeeID,
                pCsvProjectName,
                pCsvProjectRef,
                pCsvWorkType,
                pCsvStartTime,
                pCsvWorkLogQc
            };
            return fields;
        }

        #endregion ProcessCSV

        #region InputEntriesCSV

        // fields expected in the CSV
        private static string iCsvWorkType = "WorkLogType";
        private static string iCsvEmployeeID = "EmployeeID";
        private static string iCsvProjectRef = "ProjectReference";
        private static string iCsvRate = "Rate";

        private void miInputEntriesCsv_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Title = "Open New Entries CSV file";
            ofd.Filter = "CSV file (*.csv)|*.csv";
            bool? result = false;
            try
            {
                result = ofd.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error encountered: " + exc.Message);
            }
            if (result != true)
                return;

            inputCSV(ofd.FileName);
        }

        private void inputCSV(string inputFilename)
        {
            // first confirm the correct fields are present
            List<string> fieldHeaders = RecordFile.GetFileHeadersList(inputFilename);
            List<string> missingFields = new List<string>();
            foreach (string field in inputCsvRequiredFields())
            {
                if (fieldHeaders.Contains(field) == false)
                    missingFields.Add(field);
            }
            if (missingFields.Count > 0)
            {
                MessageBox.Show("Missing the following fields in the source CSV: " + String.Join(", ", missingFields));
                return;
            }

            // add line by line
            RecordFile inputFile = RecordFileFactory.OpenFile(inputFilename, true);
            int rowNum = 0;
            foreach (RecordEntry inputRecord in inputFile)
            {
                // some basic checks
                rowNum++;
                // Empty values
                if (inputRecord[iCsvProjectRef] == "" || inputRecord[iCsvWorkType] == "" || inputRecord[iCsvEmployeeID] == "" || inputRecord[iCsvRate] == "")
                {
                    MessageBox.Show("Skipping line " + rowNum.ToString() + "; One or more input values empty.");
                    continue;
                }
                // Bad work log type
                if (!GetListOfValidWorkLogTypes().Contains(inputRecord[iCsvWorkType]))
                {
                    MessageBox.Show("Skipping line " + rowNum.ToString() + "; Invalid WorkLogType value.");
                    continue;
                }
                // Duplicate entry
                string selectString = "EmployeeID = '" + inputRecord[iCsvEmployeeID] + "' AND " +
                    "ProjectReference = '" + inputRecord[iCsvProjectRef] + "' AND " +
                    "WorkLogType = '" + inputRecord[iCsvWorkType] + "'";
                DataRow[] rows = _dt.Select(selectString);
                if (rows.Count() > 0) // no matches
                {
                    MessageBox.Show("Skipping line " + rowNum.ToString() + "; Duplicate entry for Employee/ProjRef/WorkLogType.");
                    continue;
                }

                // All good, give it a go
                AddRow(inputRecord["ProjectReference"], inputRecord[iCsvWorkType], inputRecord[iCsvEmployeeID], inputRecord[iCsvRate]);
            }

            MessageBox.Show("All Done!");
        }

        private List<string> GetListOfValidWorkLogTypes()
        {
            return new List<string>
            {
                "Reviewer",
                "Reviewer - QC",
                "Reviewer - Overtime",
                "Reviewer - Team Lead",
                "Reviewer - Training",
                "Reviewer - SWAT",
                "Review Manager",
                "Review Manager - Overtime"
            };
        }

        private List<string> inputCsvRequiredFields()
        {
            return new List<string>
            {
                iCsvWorkType,
                iCsvEmployeeID,
                iCsvProjectRef,
                iCsvRate
            };
        }

        #endregion
    }
}
