﻿using EmpRatCon.SqlSetup;
using System;
using System.Windows;

namespace EmpRatCon
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static string _appPassword = "cheese";
        private static string _tableName = "EmpRates";

        Inventus.Utility.General.AdminPasswordCheck _pc;
        SqlSetupWindow _ssw;

        private void PasswordCheck(object sender, StartupEventArgs e)
        {
            if (Environment.UserName == "apiercef")
                StartMainWindow("EmpRatesTest");
            else
            {
                // Request password from user
                _pc = new Inventus.Utility.General.AdminPasswordCheck(_appPassword);
                _pc.Closing += PasswordCheck_Closing;
                _pc.Show();
            }
        }

        private void PasswordCheck_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_pc.CorrectPassword == true)
                CheckSqlSettings(); // okay cool, now ensure SQL settings have been input
        }

        private void CheckSqlSettings()
        {
            if (EmpRatCon.Properties.Settings.Default["sqlServerName"].ToString() == "") // need settings
            {
                _ssw = new SqlSetupWindow();
                _ssw.Closing += SqlSetupWindow_Closing;
                _ssw.Show();
            }
            else
            {
                StartMainWindow(_tableName); // we have settings, proceed with opening
            }
        }

        private void SqlSetupWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SqlSetupSettings settings = _ssw.Settings;
            if (settings.SqlServerName == "" || settings.SqlDatabaseName == "")
            {
                MessageBox.Show("Invalid Server or Database entered, exiting program.");
                return;
            }

            EmpRatCon.Properties.Settings.Default["sqlServerName"] = settings.SqlServerName;
            EmpRatCon.Properties.Settings.Default["sqlDatabaseName"] = settings.SqlDatabaseName;
            EmpRatCon.Properties.Settings.Default.Save();

            StartMainWindow(_tableName);
        }

        private void StartMainWindow(string tableName)
        {
            MainWindow mw = new MainWindow(tableName, EmpRatCon.Properties.Settings.Default["sqlServerName"].ToString(), EmpRatCon.Properties.Settings.Default["sqlDatabaseName"].ToString());
            Application.Current.MainWindow = mw;
            mw.Show();
        }
    }
}
