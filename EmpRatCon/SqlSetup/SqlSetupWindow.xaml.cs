﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmpRatCon.SqlSetup
{
    /// <summary>
    /// Interaction logic for SaveQueryWindow.xaml
    /// </summary>
    public partial class SqlSetupWindow : Window
    {
        public SqlSetupWindow()
        {
            InitializeComponent();
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            if (txtServer.Text == "" || txtDatabase.Text == "")
                MessageBox.Show("Please provide a non-empty server and database. Cancel to exit the application.", "Please provide a non-empty entry.");
            else
                this.Close();
        }
        
        public SqlSetupSettings Settings
        {
            get { return new SqlSetupSettings(txtServer.Text, txtDatabase.Text); }
        }

        private void btnDialogCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    public class SqlSetupSettings
    {
        public string SqlServerName { get; set; }
        public string SqlDatabaseName { get; set; }

        public SqlSetupSettings(string newServer, string newDatabase)
        {
            SqlServerName = newServer;
            SqlDatabaseName = newDatabase;
        }
    }
}
